# TofSimulations

[![Build Status](https://gitlab.com/mnkmr/TofSimulations.jl/badges/master/build.svg)](https://gitlab.com/mnkmr/TofSimulations.jl/pipelines)
[![Coverage](https://gitlab.com/mnkmr/TofSimulations.jl/badges/master/coverage.svg)](https://gitlab.com/mnkmr/TofSimulations.jl/commits/master)


## Install

Press `]` to start [Pkg REPL mode](https://docs.julialang.org/en/v1/stdlib/Pkg/), then execute:

```
(v1.1) pkg> registry add https://gitlab.com/mnkmr/MNkmrRegistry.git
(v1.1) pkg> add TofSimulations
```


## Usage

See [Tutorial jupyter notebook](https://nbviewer.jupyter.org/urls/gitlab.com/mnkmr/TofSimulations.jl/-/raw/master/HowToUse.ipynb).
