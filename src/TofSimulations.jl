module TofSimulations

export Apparatus, simulate_tof, time_extrema, binning_tof, merge_tof

using ProgressMeter:
    Progress,
    next!

using Photofragments:
    Photofragment,
    random_particle!,
    gauss,
    rakitzis

using Random:
    AbstractRNG,
    default_rng


const DEFAULT_TSTEP = 1e-9   # s

# Physical constants
const q  = 1.60217653e-19     # elementary charge
const NA = 6.0221415e23       # Avogadro number


struct Apparatus{T<:Real}
    electrodes::Vector{Pair{T,T}}

    function Apparatus(electrodes)
        n = length(electrodes)
        pos = [e.first for e in electrodes]
        vol = [e.second for e in electrodes]
        all = promote(pos..., vol...)
        new_pos = all[1:n]
        new_vol = all[n+1:end]
        T = eltype(all)
        elec = [(p => v) for (p, v) in zip(new_pos, new_vol)]
        return new{T}(elec)
    end
end
Apparatus(; VR=1200.0, VL1=1000.0, VL2=580.0, VG=0.0, VD=0.0, D1=22.0e-3, D2=26.0e-3, D3=32.0e-3, Drift=272e-3) =
    Apparatus([0.0 => VR, D1 => VL1, D1 + D2 => VL2, D1 + D2 + D3 => VG, D1 + D2 + D3 + Drift => VD])


function run(p::Photofragment, apparatus::Apparatus, xi, yi, zi, v_beam)
    acceleration(p, V1, V2, d) = p.z*q*(V1 - V2)/(p.m*d)
    linearmotion(v0, a, z) = a == 0 ? z/v0 : (-v0 + sqrt(v0^2 + 2*a*z))/a  # Return time

    vx0 = p.v*sind(p.Ω)*cosd(p.Θ)
    vy0 = p.v*cosd(p.Ω)*sind(p.Θ)
    vz0 = v_beam + p.v*cosd(p.Ω)

    tof = zero(zi)
    z = zi
    v = vz0
    n = length(apparatus.electrodes)
    prev_electrode = apparatus.electrodes[1]
    for i in 2:n
        electrode = apparatus.electrodes[i]
        z1 = prev_electrode.first
        V1 = prev_electrode.second
        z2 = electrode.first
        V2 = electrode.second
        d = abs(z1 - z2)
        a = acceleration(p, V1, V2, d)
        t = linearmotion(v, a, z2 - z)
        t <= 0 && return (NaN, NaN, NaN, NaN, vx0, vy0, vz0)
        tof += t

        prev_electrode = electrode
        z = z2
        v = v + a*t
    end
    x = xi + vx0*tof
    y = yi + vy0*tof
    return (tof, x, y, z, vx0, vy0, vz0)
end


function time_extrema(apparatus::Apparatus, z0, r0, m_au, v_beam, σv_beam, v_recoil, σv_recoil)
    σv_beam = abs(σv_beam)
    σv_recoil = abs(σv_recoil)
    v_recoil_max = v_recoil + 3*σv_recoil
    v_beam_min = v_beam - 3σv_beam
    v_beam_max = v_beam + 3σv_beam
    m = m_au*1e-3/NA
    p1 = Photofragment(m, 1, v_recoil_max, 180.0, 0.0)
    p2 = Photofragment(m, 1, v_recoil_max,   0.0, 0.0)
    o = zero(z0)
    t1, _ = run(p1, apparatus, o, o, z0 - r0, v_beam_min)
    t2, _ = run(p2, apparatus, o, o, z0 + r0, v_beam_max)
    return minmax(t1, t2)
end


function time_range(tmin, tmax, tstep)
    tmin = tstep*floor(tmin/tstep)
    tmax = tstep*ceil(tmax/tstep)
    return collect(tmin:tstep:tmax)
end


function random_initial_position(z0, r0, rng)
    r0 ≈ 0 && return zero(z0), zero(z0), z0
    r = r0*rand(rng)
    θ = acos(2*rand(rng) - 1)
    ϕ = 2π*rand(rng)
    xi = r*sin(θ)*cos(ϕ)
    yi = r*sin(θ)*sin(ϕ)
    zi = r*cos(θ) + z0
    return xi, yi, zi
end


function simulate_tof(f::Function, n, apparatus::Apparatus, z0, r0, m_au, v_beam,
                      σv_beam, v_recoil, σv_recoil, show_progress, rng)
    σv_beam = abs(σv_beam)
    σv_recoil = abs(σv_recoil)
    v_recoil_min = max(0.0, v_recoil - 3σv_recoil)
    v_recoil_max = v_recoil + 3σv_recoil
    v_beam_min = v_beam - 3σv_beam
    v_beam_max = v_beam + 3σv_beam

    # Convert mass unit a.u. -> kg
    m = m_au*1e-3/NA

    num_particle = floor(Int, n)
    prog = Progress(num_particle; dt=1)
    results = zeros(Float64, (num_particle, 8))
    p = Photofragment(m, 1, 0.0, 0.0, 0.0)
    for i in 1:num_particle
        random_particle!(rng, p, v_recoil_min, v_recoil_max)
        xi, yi, zi = random_initial_position(z0, r0, rng)
        vb = v_beam_min + (v_beam_max - v_beam_min)*rand(rng, Float64)
        t, x, y, z, vx0, vy0, vz0 = run(p, apparatus, xi, yi, zi, vb)
        show_progress && next!(prog)
        !isfinite(t) && continue

        I = f(p, vb)
        results[i, 1] = t
        results[i, 2] = I
        results[i, 3] = x
        results[i, 4] = y
        results[i, 5] = z
        results[i, 6] = vx0
        results[i, 7] = vy0
        results[i, 8] = vz0
    end
    return results
end
function simulate_tof(n, apparatus::Apparatus, z0, r0, m_au, v_beam, σv_beam, v_recoil,
                      σv_recoil, Γ, Δ, Φ, α, χ, ϕ_μd, c1, c2;
                      show_progress=true, rng=default_rng())
    function f(p::Photofragment, vb)
        I_recoil = gauss(p.v, v_recoil, σv_recoil)*rakitzis(p.Ω, p.Θ, Γ, Δ, Φ, α, χ, ϕ_μd, c1, c2)
        I_beam = vb^2 * exp(-(vb - v_beam)^2/(2*σv_beam^2)) / (2*σv_beam^4)
        return I_recoil*I_beam
    end
    return simulate_tof(f, n, apparatus, z0, r0, m_au, v_beam, σv_beam, v_recoil,
                        σv_recoil, show_progress, rng)
end
function simulate_tof(n, apparatus::Apparatus, z0, r0, m_au, v_beam, σv_beam,
                      v_recoil, σv_recoil, Γ, β, show_progress=true, rng=default_rng())
    χ = acosd(sqrt((β + 1)/3))
    function f(p::Photofragment, vb)
        I_recoil = gauss(p.v, v_recoil, σv_recoil)*rakitzis(p.Ω, p.Θ, Γ, 0, 0, 0, χ, 0, 0, 0)
        I_beam = vb^2 * exp(-(vb - v_beam)^2/(2*σv_beam^2)) / (2*σv_beam^4)
        return I_recoil*I_beam
    end
    return simulate_tof(f, n, apparatus, z0, r0, m_au, v_beam, σv_beam, v_recoil,
                        σv_recoil, show_progress, rng)
end


function binning_tof(results, tmin, tmax; tstep=DEFAULT_TSTEP)
    n = size(results, 1)
    tof = time_range(tmin, tmax, tstep)
    t0 = tof[1]
    spectrum = zeros(Float64, length(tof))
    for i in 1:n
        t, I, _ = results[i, :]
        idx = floor(Int, (t - t0)/tstep) + 1
        spectrum[idx] += I
    end
    return tof, spectrum
end
function binning_tof(f::Function, results, t1, t2; tstep=DEFAULT_TSTEP)
    n = size(results, 1)
    tof = time_range(t1, t2, tstep)
    spectrum = zeros(Float64, length(tof))
    t0 = tof[1]
    for i in 1:n
        t, I, _ = results[i]
        idx = floor(Int, (t - t0)/tstep) + 1
        spectrum[idx] .+= f(t, tof, I)
    end
    return tof, spectrum
end


# utility function to merge two TOF spectra
function merge_tof(t1, s1, w1, t2, s2, w2, tstep)
    tmin = min(t1[begin], t2[begin]) - tstep
    tmax = max(t1[end], t2[end]) + tstep
    tof = collect(tmin:tstep:tmax)
    merged = zeros(Float64, (length(tof), 3))

    function migrate!(n, target, t, s, w)
        thr = tstep/2
        i = findfirst(abs.(tof .- t[1]) .< thr)
        for insertion in s
            fraction = insertion*w
            target[i, 1] += fraction
            target[i, n] = fraction
            i += 1
        end
    end
    migrate!(2, merged, t1, s1, w1)
    migrate!(3, merged, t2, s2, w2)
    return tof, merged
end
function merge_tof(t1, s1, w1, t2, s2, w2)
    tstep1 = abs(t1[begin+1] - t1[begin])
    tstep2 = abs(t2[begin+1] - t2[begin])
    tstep = min(tstep1, tstep2)
    return merge_tof(t1, s1, w1, t2, s2, w2, tstep)
end


end # module
